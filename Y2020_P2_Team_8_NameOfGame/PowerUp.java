import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class PowerUp here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class PowerUp extends Actor
{
    /**
     * Act - do whatever the PowerUp wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        GreenfootImage img = getImage();
        setLocation(getX(), getY() + 2);
        int count = 0;
        
        if(getY() > getWorld().getHeight()) {
            setLocation(Greenfoot.getRandomNumber(getWorld().getWidth()), 0 + img.getHeight()/2);
        }
        
        if(isTouching(Player.class)) {
            World world = getWorld();
            Obstacle obs = new Obstacle();
            GreenfootImage img2 = obs.getImage();
            img2.scale(40,40);
            world.addObject(obs,Greenfoot.getRandomNumber(world.getWidth()), 0 + img2.getHeight()/2);
            
            setLocation(Greenfoot.getRandomNumber(getWorld().getWidth()), 0 + img.getHeight()/2);
            
        }
        

        
        

        
        
        
    }    
}
