import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class Obstacle here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Obstacle extends Actor
{
    /**
     * Act - do whatever the Obstacle wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        GreenfootImage img = getImage();
        setLocation(getX(), getY() + 5);
        
        
        if(getY() > getWorld().getHeight()) {
            setLocation(Greenfoot.getRandomNumber(getWorld().getWidth()), 0 + img.getHeight()/2);
        }
        
        if(isTouching(Player.class)) {
            World world = getWorld();
            Lost lost = new Lost();
            world.addObject(lost,world.getWidth()/2,world.getHeight()/2);
            
        }
        
    }    
}
