import greenfoot.*;
/**
 * Write a description of class GameOverState here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class GameOverState extends State 
{
    // instance variables - replace the example below with your own
    private int x;

    /**
     * Constructor for objects of class GameOverState
     */
    public GameOverState(RacingWorld x)
    {
        super(x);
    }
    
    public void onSet(){
        Lost screen = new Lost();
        screen.getImage().scale(200 , 200);
        getWorld().addObject(screen, getWorld().getWidth()/2  , getWorld().getHeight()/2);
    }
    
    public void onAct(){
        if(Greenfoot.isKeyDown("space")){
            State titleState = new TitleState(getWorld());
            getWorld().setState(titleState);
        }
    }
    
}
