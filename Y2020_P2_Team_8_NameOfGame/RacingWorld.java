import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class MyWorld here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public abstract class RacingWorld extends World
{
    private State current = null;
    /**
     * Constructor for objects of class MyWorld.
     * 
     */
    public RacingWorld(int height, int width, int cellSize, boolean bounded)
    {    
        // Create a new world with 600x400 cells with a cell size of 1x1 pixels.
        super(600, 400, 1,false); 
    }
    
    public RacingWorld(int height, int width, int cellSize){
        super(600, 500, 1);
    }
    
    public void setState(State x){
        if(current != null){
            current.onRemove();
        }
        current = x;
        current.onSet();
    }

    @Override
    public void act(){
        if(current != null){
            current.onAct();
        }
    }
}
