import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
/**
 * Write a description of class TitleState here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class TitleState extends State 
{
    // instance variables - replace the example below with your own
    private boolean spacePressed = false;

    /**
     * Constructor for objects of class TitleState
     */
    public TitleState(RacingWorld x)
    {
        super(x);
    }
    upTxt txt1 = new upTxt();
    downText txt2 = new downText();
    @Override
    public void onSet(){
        Title object = new Title();
        getWorld().addObject(object , getWorld().getWidth()/2 , getWorld().getHeight()/2);
        //getWorld().getBackground().drawImage(new GreenfootImage("Collect PowerUps to Increase Difficulty ", 30, null, null), 50, 40);
        //getWorld().getBackground().drawImage(new GreenfootImage("Press Space to Continue", 30, null, null), 163, 325);
        
        getWorld().addObject(txt1 , getWorld().getWidth()/2 , 50);
        
        getWorld().addObject(txt2 , getWorld().getWidth()/2 , getWorld().getHeight()/2 + 145);
    }

    @Override
    public void onAct(){
        if (Greenfoot.isKeyDown("space")) {
            spacePressed = true; // spacebar has been pressed
        } else if (spacePressed) { // spacebar is no longer pressed, but it was just released
            spacePressed = false; // reset spacePressed to false since we are handling the keyup event
            State gameState = new GameState(getWorld());
            getWorld().setState(gameState);
            
        }

    }

}
