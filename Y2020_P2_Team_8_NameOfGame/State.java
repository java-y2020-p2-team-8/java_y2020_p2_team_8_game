/**
 * Write a description of class State here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public abstract class State  
{
    // instance variables - replace the example below with your own
    RacingWorld something;

    /**
     * Constructor for objects of class State
     */
    public State(RacingWorld x)
    {
        something = x;
    }

    /**
     * An example of a method - replace this comment with your own
     * 
     * @param  y   a sample parameter for a method
     * @return     the sum of x and y 
     */
    public RacingWorld getWorld()
    {
        return something;
    }
    
    public abstract void onSet();
    
    public void onRemove(){
        getWorld().removeObjects(getWorld().getObjects(null));
    }
    
    public void onAct(){
        
    }
}
