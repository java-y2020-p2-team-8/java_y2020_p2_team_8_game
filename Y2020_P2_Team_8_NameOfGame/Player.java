import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class Player here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Player extends Actor
{
    /**
     * Act - do whatever the Player wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public Player(){
        getImage().scale(50,50);
    }
    int x = 5;
    int y = 5;
    
    /**
     * Act - do whatever the Player wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
      
        if(Greenfoot.isKeyDown("left")){
            move(-x);

        }
        if(Greenfoot.isKeyDown("right")){
            move(x);
        }
        if(Greenfoot.isKeyDown("up")){
            setLocation(getX(), getY() - y);
        }
        if(Greenfoot.isKeyDown("down")){
            setLocation(getX(), getY() + y);
        }
        if(getY() <= getWorld().getHeight() * 0.5){
            setLocation(getX(), getWorld().getHeight()/2);
        }
        if(getY() >= getWorld().getHeight()){
            setLocation(getX(), getWorld().getHeight() - 5);
        }
        if(getX() >= getWorld().getWidth()){
            setLocation(getWorld().getWidth() - 5, getY());
        }
        if(getX() <= 0){
            setLocation(5, getY());
        }
        

        

        
        

    } 

}
