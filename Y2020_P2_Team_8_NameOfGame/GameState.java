import java.util.List;
import greenfoot.*;
/**
 * Write a description of class GameState here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class GameState extends State 
{
    // instance variables - replace the example below with your own
    private int x;
    private int check = 0;
    /**
     * Constructor for objects of class GameState
     */
    public GameState(RacingWorld x){
        super(x);
    }
    World world = getWorld();
    public void onSet(){
        Player player = new Player();
        world.addObject(player,world.getWidth()/2,world.getHeight()-player.getImage().getHeight()/2);
        Obstacle obstacle = new Obstacle();
        GreenfootImage img = obstacle.getImage();
        img.scale(40,40);
        world.addObject(obstacle,Greenfoot.getRandomNumber(world.getWidth()), 0 + img.getHeight()/2);
        
        Obstacle obstacle2 = new Obstacle();
        GreenfootImage img2 = obstacle2.getImage();
        img2.scale(40,40);
        world.addObject(obstacle2,Greenfoot.getRandomNumber(world.getWidth()), 0 + img2.getHeight()/2);
        
        PowerUp power = new PowerUp();
        GreenfootImage powerimg = power.getImage();
        powerimg.scale(30,30);
        world.addObject(power,Greenfoot.getRandomNumber(world.getWidth()), 0 + powerimg.getHeight()/2);
    }

    @Override
    public void onAct(){

    }
}


