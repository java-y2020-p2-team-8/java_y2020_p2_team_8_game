Game:

The game is a game where there is a car and the player has to control the car.
Objects fall out of the sky and the player has to dodge them. However, there
are also power ups that the player can get, which will add more obstacles. If the player hits the obstacles,
there is a game overscreen and the player has to reset to play again.

Classes:

The game will require a car/player class, an obstacle class, a score class, 
a level class, and a power-up class.

Class Assignment:

Sumanth: obstsacle class, score class, and level class
Pranav: player class
Utkarsh: power-up class




Members: 
Pranav Bollineni
Sumanth Sureshkumar
Utkarsh Nandy


PRANAV'S JOURNAL:
	Today I learned how to use GIT and what it's used for: team projects. 
	For this project I want to create a top down obstacle avoidance racing game.
	5/10: We assigned our classes and started work on them
	5/17: We created the core mechanics of the game, such as the function of the power up and the lose game screen.
	5/25: Did the states and instructions
	5/31: Checked the final adjustments and if game worked.

SUMANTH'S JOURNAL:
	5/1: I learned how git works and how to deal with merge conflicts.
		 I want to make a racing game.
	5/10: Decided what game we want to do, and assigned classes to each team member.

# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact